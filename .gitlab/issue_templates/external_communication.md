<!-- ISSUE TITLING: use the form "YYYY-MM-DD: Scheduled Maintenance: Briefly describe the reason for the maintenance"

For example: https://gitlab.com/gitlab-com/gl-infra/production/-/issues/1993

-->

# Scheduled Maintenance

### For IMOC

#### Timing

:clock9: **Planned Start:** {+YYYY-MM-DD 00:00 UTC+}

:clock5: **Planned End:** {+YYYY-MM-DD 00:00 UTC+}

#### Infrastructure

<details>
<summary>:building_construction: {+Affected Infrastructure+}</summary>

Check all that apply:

- [ ] Website
- [ ] API
- [ ] Git (SSH and HTTPS)
- [ ] Pages
- [ ] Registry
- [ ] CI/CD
- [ ] Background Processing
- [ ] Support Services
- [ ] packages.gitlab.com
- [ ] customers.gitlab.com
- [ ] version.gitlab.com
- [ ] forum.gitlab.com
- [ ] Windows Runners (Beta)

</details>

#### Reminder Emails

Select if and when we should send email reminders to users through our status page about when the maintenance will take place.

- [ ] {+Send email notifications now+} _(when the maintenance is created)_
- [ ] {+Send email notifications 72 hours before planned start+}
- [ ] {+Send email notifications 24 hours before planned start+}
- [ ] {+Send email notifications 1 hour before planned start+}
- [ ] {+None of the above+}

#### Should this maintenance appear on our Status Page?

>:warning: **If {+Yes+}, add the ~"CMOC Required" label to this issue** :warning:

- [ ] {+Yes+}
- [ ] {+No+}

#### Will the CMOC need to be actively engaged during the maintenance window?

- [ ] {+Yes+}
- [ ] {+No+}

#### Maintenance Summary

{+Provide a high-level summary of the maintenance and its purpose.+}

<!--
Write up a general summary for this scheduled maintenance: what is it about, whether it'll require downtime, mention if there is a clear rollback step in case the change fail. Use clear, complete sentences. Provide as much information to help customers as possible. More detail and links can be added in the next sections.

Additionally, you should clearly state when this maintenance is scheduled (in UTC and perhaps any other important timezone for customers), its duration and how the customers will be able to follow updates on this maintenance via status.io for example, and frequency of the updates.
-->

#### Why are we carrying out this maintenance?

{+Provide reasons that justify this maintenance occurring.+}

<!--
Main reasons that justify this scheduled maintenance. Perhaps also mention how customers will be able to benefit of this change - new features, improvements, more stability, etc.

Use clear, complete sentences. Provide as much information to help customers, as possible, including links to public issues and assets. 
-->

#### Will it require downtime?

- [ ] {+Yes+}
- [ ] {+No+}

**If yes, details:**

<!--
Main and convincing reasons that justify the need for downtime, if downtime is required.

Use clear, complete sentences. Provide as much information to help customers, as possible, including links to public issues and assets. 
-->

#### Do customers have to prepare or do something for this maintenance?

{+Detail what customers or users may need to do in prepartion for this maintenance.+}

<!--
Summarize how customer can determine if any action is required from their side, and if so explain in detail.
-->

#### Additional Information

{+Provide any additional information or link that may be helpful.+}

<!--
Provide additional info and links to the non-confidential EPICs or Issues further explaining and providing context on this change.

Link also any existing **design documents** explaining in depth the changes involved in this maintenance.

Lastly, perhaps add a final mention on where to direct people to get more help or send their feedback about this upcoming change.
-->

### For CMOC

- [ ] **Maintenance Created:** {+LINK+}

/label ~"Scheduled Maintenance"
